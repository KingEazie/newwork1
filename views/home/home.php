<section id="dashboard" class="container-fluid">
    <div class="row">
        <div class="col-sm-12 col-md-4">
            <div class="card bg-primary">
                <div class="card-body">
                    <h2 class="text-white">Individuals <span class="pull-right"> <?= ($individuals!==false)? $individuals:0 ?> </span></h2>
                </div>
                <div class="card-footer">
                    <a href="individual" data-query="individual" data-output="#page-display" data-dest="<?=__URL__.'/views/pages/entity.php'?>" class="spec-ajax text-white-50">View Details</a>
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-md-4">
            <div class="card bg-warning">
                <div class="card-body">
                    <h2 class="text-white">Companies <span class="pull-right"> <?= ($companies!==false)? $companies:0 ?> </span></h2>
                </div>
                <div class="card-footer">
                    <a href="company" data-query="company" data-output="#page-display" data-dest="<?=__URL__.'/views/pages/entity.php'?>" class="spec-ajax text-white-50">View Details</a>
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-md-4">
            <div class="card bg-success">
                <div class="card-body">
                    <h2 class="text-white">Screening <span class="pull-right"> <?= ($screening!==false)? $screening:0 ?> </span></h2>
                </div>
                <div class="card-footer">
                    <a href="screenings" data-query="screenings" data-output="#page-display" data-dest="<?=__URL__.'/views/pages/screenings.php'?>" class="spec-ajax text-white-50">View Details</a>
                </div>
            </div>
        </div>
    </div>
    
    <div class="row aj-padding-top-20p">
        <div class="col-sm-12 col-md-6">
            <div class="card bg-info">
                <div class="card-body">
                    <h2 class="text-white">Total Transactions Screened <span class="pull-right"> <?= !is_null($total_tranx)? number_format($total_tranx,"0",".",",") : "N/A" ?> </span></h2>
                </div>
                <div class="card-footer">
                    <a href="t_screening" class="spec-ajax text-white-50" data-query="t_screening" data-output="#page-display" data-dest="<?=__URL__.'/views/pages/tscreening.php'?>">View Details</a>
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-md-6">
            <div class="card bg-danger">
                <div class="card-body">
                    <h2 class="text-white">Flagged Transactions <span class="pull-right"> <?= !is_null($flagged_tranx)? number_format($flagged_tranx,"0",".",",") : "N/A" ?> </span></h2>
                </div>
                <div class="card-footer">
                    <a href="f_screening" class="spec-ajax text-white-50" data-query="f_screening" data-output="#page-display" data-dest="<?=__URL__.'/views/pages/fscreening.php'?>">View Details</a>
                </div>
            </div>
        </div>
    </div>

    <div class="aj-padding-top-20p">
        <div class="col bg-light card">
            <div class="card-header bg-transparent">
                <h4 class="text-danger">Undecided Screening</h4>
            </div>
            <div>
                <? if ($pending!==false) { $j = 0; ?>
                    <table class="table table-responsive-sm table-hover">
                        <thead class="table-borderless">
                            <th>Id</th>
                            <th>Screen Type</th>
                            <th>Screening Id</th>
                            <th>Customer Id</th>
                            <th>Date Done</th>
                        </thead>
                        <tbody>
                            <? foreach ($pending as $pend) { ?>
                                <tr id="<?= $pend['id'] ?>" class="cursor spec-ajax" data-value="<?= $pend['customer_id'] ?>" data-query="<?= ($pend['screen_type']=='COMPANY')? 'company-entity' : 'individual-entity' ?>" data-output=".modal-body" data-dest="<?= __URL__.'/actions/pages.actions.php' ?>" data-toggle="modal" data-title="SCREENING INFORMATION">
                                    <td><?= ++$j.'.'; ?></td>
                                    <td><?= $pend['screen_type'] ?></td>
                                    <td><?= $pend['screening_id'] ?></td>
                                    <td><?= $pend['customer_id'] ?></td>
                                    <td><?= $pend['date_done'] ?></td>
                                </tr>
                            <? } ?>
                        </tbody>
                    </table>
                <? } else { ?>
                    <p class="text-center aj-padding-top-20p">No Undecided Screening Available!</p>
                <? } ?>
            </div>
        </div>
    </div>
</section>