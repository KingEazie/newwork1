<?php
// include controller
include("../../layout/definition.php");
// include transactions
include("../../controllers/account.controller.php");

$page = new Account;
?>

<div class="container-fluid" id="user">
    <div class="user d-none">
        <form class="form" data-dest="<?php echo __URL__.'/actions/account.actions.php' ?>" data-output=".modal-body">
            <div class="form-group">
                <label for="first_name">First Name</label>
                <input class="form-control validate" type="text" id="first_name" name="first_name" required>
            </div>
            <div class="form-group">
                <label for="last_name">Last Name</label>
                <input class="form-control validate" type="text" id="last_name" name="last_name" required>
            </div>
            <div class="form-group">
                <label for="username">Username</label>
                <input class="form-control validate" type="text" id="username" name="username" required>
            </div>
            <div class="form-group">
                <label for="email">Email</label>
                <input class="form-control validate" type="email" id="email" name="email" required>
            </div>
            <div class="form-group">
                <select class="form-control" required name="role">
                    <optgroup label="set Role">
                        <option value="1">Restricted</option>
                        <option value="3">Administration</option>
                        <option value="5">Management</option>
                        <option value="7">Senior Management</option>
                        <option value="9">Administrator</option>
                        <option value="10">Technical</option>
                    </optgroup>
                </select>
            </div>
            <div class="form-group">
                <select class="form-control" required name="status">
                    <optgroup label="set status">
                        <option value="1">Active</option>
                        <option value="0">Inactive</option>
                        <option value="-1">Suspend</option>
                    </optgroup>
                </select>
            </div>
            <div class="form-group">
                <input type="hidden" name="user-confirm" value="aj">
                <button type="submit" class="btn btn-outline-success form-control">Create User</button>
            </div>
        </form>
    </div>
    <?php
        if (isset($_POST['query']) && $_POST['query'] == "account") {
            $info  = $page->index($_SESSION['id']); ?>
        <form data-dest="<?php echo __URL__.'/actions/account.actions.php' ?>" data-output=".modal-body" class="form">
            <div class="form-group">
                <label for="first_name">First Name</label>
                <input class="form-control validate" type="text" id="first_name" name="first_name" value="<?= $info['first_name'] ?>" required>
            </div>
            <div class="form-group">
                <label for="last_name">Last Name</label>
                <input class="form-control validate" type="text" id="last_name" name="last_name" value="<?= $info['last_name'] ?>" required>
            </div>
            <div class="form-group">
                <label for="username">Username</label>
                <input class="form-control validate" type="text" id="username" name="username" value="<?= $info['username'] ?>" required>
            </div>
            <div class="form-group">
                <label for="email">Email</label>
                <input class="form-control validate" type="email" id="email" name="email" value="<?= $info['email'] ?>" required>
            </div>
            <div class="form-group">
                <label for="password">Password</label>
                <input class="form-control validate" type="password" id="password" name="password" required>
            </div>
            <div class="form-group">
                <a href="#" class="text-info spec-ajax" data-query="change-password" data-output=".modal-body" data-dest="<?= __URL__.'/views/home/account.php' ?>" data-toggle="modal"><i class="fa fa-shield"></i> Change Password?</a>
            </div>
            <div class="form-group center-align">
                <input class="form-control" type="hidden" name="editaccount" value="<?= $info['id'] ?>">
                <button type="submit" class="form-control btn btn-outline-primary">CONFIRM</button>
            </div>
        </form>
<?  } elseif (isset($_POST['query']) && $_POST['query'] == "change-password") { ?>
        <form data-dest="<?php echo __URL__.'/actions/account.actions.php' ?>" data-output=".modal-body" class="form">
            <div class="form-group">
                <label for="first_name">Old Password</label>
                <input class="form-control validate" type="password" id="opass" name="opass" required>
            </div>
            <div class="form-group">
                <label for="last_name">New Password</label>
                <input class="form-control validate" type="password" id="npass" name="npass" required>
            </div>
            <div class="form-group">
                <label for="password">Confirm Password</label>
                <input class="form-control validate" type="password" id="rpass" name="rpass" required>
            </div>
            <div class="form-group center-align">
                <input class="form-control" type="hidden" name="passchange" value="<?= $info['id'] ?>">
                <button type="submit" class="form-control btn btn-outline-primary">CONFIRM</button>
            </div>
        </form>
<?  } elseif (isset($_POST['query']) && $_POST['query'] == "users") {
            $users = $page->fetch_users(); ?>
        <div>
            <button class="pull-right btn-outline-info rounded aj-margin-bottom-20p spec-ajax" data-dest="<?= __URL__.'/views/home/account.php' ?>" data-extend-view=".user" data-parent="#user" data-output=".modal-body" data-toggle="modal" data-title="NEW USER" return="true">
                <i class="fa fa-user-secret"></i> Add User
            </button>
            <table class="table table-bordered table-hover">
                <thead class="bg-danger">
                    <tr class="text-white">
                        <th>No.</th>
                        <th>Full Name</th>
                        <th>Username</th>
                        <th>Email</th>
                        <th>Role</th>
                        <th>Status</th>
                        <th class="text-center">Action</th>
                    </tr>
                </thead>
                <tbody id="user-table">
                <? if (isset($users) && $users !== false) {
                    $i = 0;
                    foreach ($users as $user) {
                        ++$i; ?>
                        <tr>
                            <td><?= $i . '.'; ?></td>
                            <td><?= $user['first_name'].' '.$user['last_name'] ?></td>
                            <td><?= $user['username'] ?></td>
                            <td><?= $user['email'] ?></td>
                            <td>
                                <? if ($user['role'] == 10)
                                    echo 'Technical';
                                elseif ($user['role'] == 9)
                                    echo 'Administrator';
                                elseif ($user['role'] == 7)
                                    echo 'Senior Management';
                                elseif ($user['role'] == 5)
                                    echo 'Management';
                                elseif ($user['role'] == 3)
                                    echo 'Administration';
                                else
                                    echo 'Restricted';
                                ?>
                            </td>
                            <td>
                                <? if ($user['status'] == 1)
                                    echo '<span class="text-success">Active</span>';
                                elseif ($user['status'] == 0)
                                    echo '<span class="text-warning">Inactive</span>';
                                else
                                    echo '<span class="text-danger">Suspended</span>';
                                ?>
                            </td>
                            <td class="text-center">
                                <a href="#" data-value="<?= $user['id'] ?>" class="spec-ajax aj-padding-right" data-query="reset-user" data-output=".modal-body" data-dest="<?= __URL__.'/actions/account.actions.php' ?>" data-toggle="modal" data-title="PASSWORD RESET"><i class="fa fa-rotate-left"></i></a>
                                <a href="#" id="<?= $user['id'] ?>" class="spec-ajax" data-query="edit-user" data-output=".modal-body" data-dest="<?= __URL__.'/actions/account.actions.php' ?>" data-toggle="modal" data-title="EDIT USER DETAILS"><i class="fa fa-cogs"></i></a>
                            </td>
                        </tr>
                    <? }
                } else { ?>
                    <tr>
                        <td colspan=7 class="text-center text-muted">No User(s) Found!</td>
                    </tr>
                <?   } ?>
                </tbody>
            </table>
        </div>
        <?   } ?>
</div>
