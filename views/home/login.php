<?php
// include controller
include("../../layout/definition.php");

include("../../controllers/login.controller.php");

// include head
include("../../layout/head.php");

// include portal
?>

<section class="container-fluid space">
    <div class="card col-md-8 offset-md-2 col-sm-12">
        <div class="card-header bg-danger row text-center">
            <h1 class="text-white">Compliance Portal</h1>
        </div>
        <div id="feedback" class="aj-padding-top-20p"></div>
        <form data-dest="<?= __URL__.'/actions/login.actions.php' ?>" data-output="#feedback" class="form">
            <div class="form-group aj-padding-top-20p">
                <input type="text" id="username" name="username" required class="form-control" placeholder="Email or Username">
            </div>
            <div class="form-group">
                <input type="password" id="passphrase" name="passphrase" required class="form-control" placeholder="Password">
            </div>
            <div class="form-group text-center">
                <input type="hidden" name="login" value="login">
                <button class="btn-outline-danger" style="width: 50%" type="submit">LOGIN</button>
            </div>
        </form>
    </div>
</section>

<?php
    include("../../layout/modal.php");