<?php
// include controller
include("../../layout/definition.php");
// include transactions
include("../../controllers/pages.controller.php");

$page = new Page;
$screens = $page->fetch_screenings();
?>
<section id="screenings">
    <div class="container-fluid">
        <?php if (isset($_POST['query']) && $_POST['query'] == "screenings") { ?>
            <table class="table table-hover table-striped table-responsive-sm">
                <thead class="table-warning">
                    <th>No.</th>
                    <th>Screen Type</th>
                    <th>Screening ID</th>
                    <th>Customer ID</th>
                    <th>Overall Status</th>
                    <th>Date Done</th>
                    <th class="text-center">Decided</th>
                </thead>
                <tbody>
                <? if($screens !== false) {
                    $i = 0;
                    foreach($screens as $screen) { ?>
                        <tr id="<?= $screen['id'] ?>" class="cursor <?= ($screen['decided']==0)? 'spec-ajax':'' ?>" data-value="<?= $screen['customer_id'] ?>" data-query="<?= ($screen['screen_type']=='COMPANY')? 'company-entity' : 'individual-entity' ?>" data-output=".modal-body" data-dest="<?= __URL__.'/actions/pages.actions.php' ?>" data-toggle="modal" data-title="SCREENING INFORMATION">
                            <td><?= ++$i.'.' ?></td>
                            <td><?= $screen['screen_type'] ?></td>
                            <td><?= $screen['screening_id'] ?></td>
                            <td><?= $screen['customer_id'] ?></td>
                            <td><?= $screen['overall_status'] ?></td>
                            <td><?= date("jS M. Y", strtotime($screen['date_done'])) ?></td>
                            <td class="text-center"><?= ($screen['decided']==0)? 'NO':'YES' ?></td>
                       </tr>
                    <?  } } ?>
                </tbody>
            </table>
        <?php } ?>
    </div>
</section>
