<?php
	// include controller
	include("../../layout/definition.php");
	// include transactions
	include("../../controllers/pages.controller.php");

	$page = new Page;
	list($individuals,$companies,$nationalities) = $page->fetch_entities();
?>
<section id="entity">
    <div class="d-none individual">
        <form data-dest="<?php echo __URL__.'/actions/pages.actions.php' ?>" data-output=".modal-body" class="form">
            <div class="form-group">
                <label for="first_name">First Name</label>
                <input class="form-control validate" type="text" id="first_name" name="first_name" required>
            </div>
            <div class="form-group">
                <label for="middle_name">Middle Name</label>
                <input class="form-control validate" type="text" id="middle_name" name="middle_name">
            </div>
            <div class="form-group">
                <label for="last_name">Last Name</label>
                <input class="form-control validate" type="text" id="last_name" name="last_name" required>
            </div>
            <div class="row">
                <div class="form-group col-sm-4">
                    <label for="gender">Gender</label>
                    <select id="gender" name="gender" class="form-control">
                        <optgroup label="Gender">
                            <option value="">Unknown</option>
                            <option value="MALE">Male</option>
                            <option value="FEMALE">Female</option>
                        </optgroup>
                    </select>
                </div>
                <div class="form-group col-sm-8">
                    <label for="dob">DOB</label>
                    <input class="form-control validate" type="date" id="dob" name="dob">
                </div>
            </div>
            <div class="form-group">
                <label for="nationality">Nationality</label>
                <select id="nationality" name="nationality" class="form-control" required>
                    <optgroup label="Nationality">
                        <? foreach ($nationalities as $nationality) { ?>
                         <option value="<?= $nationality['id'] ?>"><?= $nationality['name'] ?></option>
                        <? } ?>
                    </optgroup>
                </select>
            </div>
            <div class="form-group">
                <label for="mobile">Mobile</label>
                <input class="form-control validate" type="text" id="mobile" name="mobile" placeholder="Eg. +233 xx xxx xxxx">
            </div>
            <div class="form-group">
                <label for="email">Email</label>
                <input class="form-control validate" type="email" id="email" name="email" required>
            </div>
            <div class="form-group center-align">
                <input class="form-control" type="hidden" name="signindividual" value="aj">
                <button type="submit" class="form-control btn btn-outline-primary">CREATE</button>
            </div>
        </form>
    </div>

    <div class="d-none company">
        <form data-dest="<?php echo __URL__.'/actions/pages.actions.php' ?>" data-output=".modal-body" class="form">
            <div class="form-group">
                <label for="company_name">Company Name</label>
                <input class="form-control validate" type="text" id="company_name" name="company_name" required>
            </div>
            <div class="form-group">
                <label for="email">Email</label>
                <input class="form-control validate" type="email" id="email" name="email" required>
            </div>
            <div class="form-group">
                <label for="telephone">Telephone</label>
                <input class="form-control validate" type="text" id="telephone" name="telephone" placeholder="Eg. +233 xx xxx xxxx">
            </div>
            <div class="form-group">
                <label for="incorporation_number">Incorporation Number</label>
                <input class="form-control validate" type="text" id="incorporation_number" name="incorporation_number">
            </div>
            <div class="form-group">
                <label for="incorporation_type">Incorporation Type</label>
                <select id="incorporation_type" name="incorporation_type" class="form-control">
                    <optgroup label="Incorporation Type">
                        <option value="">Unknown</option>
                        <option value="SOLE_TRADER">Sole Trader</option>
                        <option value="PRIVATE_LIMITED">Private Limited</option>
                        <option value="LIMITED_LIABILITY_PARTNERSHIP">Limited Liability Partnership</option>
                        <option value="PUBLIC_LIMITED">Public Limited</option>
                    </optgroup>
                </select>
            </div>
            <div class="form-group">
                <label for="incorporation_country">Incorporation Country</label>
                <select id="incorporation_country" name="incorporation_country" class="form-control" required>
                    <optgroup label="Incorporation Country">
                        <? foreach ($nationalities as $nationality) { ?>
                            <option value="<?= $nationality['id'] ?>"><?= $nationality['name'] ?></option>
                        <? } ?>
                    </optgroup>
                </select>
            </div>
            <div class="form-group">
                <label for="business_purpose">Business Purpose</label>
                <select id="business_purpose" name="business_purpose" class="form-control">
                    <optgroup label="Business Purpose">
                        <option value="">Unknown</option>
                        <option value="REGULATED_ENTITY">Regulated Entity</option>
                        <option value="PRIVATE_ENTITY">Private Entity</option>
                        <option value="UNREGULATED_FUND">Unregulated Fund</option>
                        <option value="TRUST">Trust</option>
                        <option value="FOUNDATION">Foundation</option>
                        <option value="RELIGIOUS_BODY">Religious Body</option>
                        <option value="GOVERNMENT_ENTITY">Government Entity</option>
                        <option value="CHARITY">Charity</option>
                        <option value="CLUB">Club</option>
                        <option value="SOCIETY">Society</option>
                    </optgroup>
                </select>
            </div>
            <div class="form-group center-align">
                <input class="form-control" type="hidden" name="signcompany" value="aj">
                <button type="submit" class="form-control btn btn-outline-success">CREATE</button>
            </div>
        </form>
    </div>

	<div class="container-fluid">
        <?php if (isset($_POST['query']) && $_POST['query'] == "individual") { ?>
            <button class="pull-right btn-outline-success rounded aj-margin-bottom-20p spec-ajax" data-dest="<?= __URL__.'/views/pages/entity.php' ?>" data-extend-view=".individual" data-parent="#entity" data-output=".modal-body" data-toggle="modal" data-title="NEW INDIVIDUAL" return="true">
                <i class="fa fa-user-plus"></i> Add Individual
            </button>
            <table class="table table-hover table-striped table-responsive-sm aj-padding-top-20p">
                <thead class="table-info">
                    <th>No.</th>
                    <th>Full Name</th>
                    <th>Individual ID</th>
                    <th>Nationality</th>
                    <th>Mobile</th>
                    <th>Email</th>
                    <th class="text-center">Screenings</th>
                    <th>Status</th>
                    <th class="text-center">Actions</th>
                </thead>
                <tbody>
                    <? if($individuals !== false) {
                        $i = 0;
                         foreach($individuals as $individual) { ?>
                        <tr class="cursor">
                            <td><?= ++$i.'.' ?></td>
                            <td><?= $individual['first_name'].' '.$individual['middle_name'].' '.$individual['last_name'] ?></td>
                            <td><?= $individual['individual_id'] ?></td>
                            <td><?= isset($individual['nationality'])? $individual['nationality'] : 'N/A' ?></td>
                            <td><?= isset($individual['mobile'])? $individual['mobile'] : 'N/A' ?></td>
                            <td><?= $individual['email'] ?></td>
                            <td class="text-center"><?= $individual['screenings'] ?></td>
                            <td><? if ($individual['status']==1)  { echo "<span class='text-success'>Approved</span>"; } elseif ($individual['status']==-1) { echo "<span class='text-danger'>Disapproved</span>"; } else { echo "<span class='text-warning'>Pending</span>"; } ?></td>
                            <td class="text-center">
                                <a class="spec-ajax aj-padding-right" href="edit" data-value="<?= $individual['individual_id'] ?>" data-query="individual-edit" data-output=".modal-body" data-dest="<?= __URL__.'/actions/pages.actions.php' ?>" data-toggle="modal" data-title="EDIT INDIVIDUAL"><i class="fa fa-edit"></i></a>
                                <a class="spec-ajax" href="screen" data-value="<?= $individual['individual_id'] ?>" data-query="individual-screen" data-output=".modal-body" data-dest="<?= __URL__.'/actions/pages.actions.php' ?>" data-toggle="modal" data-title="SCREEN INDIVIDUAL"><i class="fa fa-lock"></i></a>
                            </td>
                        </tr>
                    <?  } } ?>
                </tbody>
            </table>
        <? } elseif (isset($_POST['query']) && $_POST['query'] == "company") { ?>
                <button class="pull-right btn-outline-primary rounded aj-margin-bottom-20p spec-ajax" data-dest="<?= __URL__.'/views/pages/entity.php' ?>" data-extend-view=".company" data-parent="#entity" data-output=".modal-body" data-toggle="modal" data-title="NEW COMPANY" return="true">
                    <i class="fa fa-building-o"></i> Add Company
                </button>
                <table class="table table-hover table-striped table-responsive-sm">
                    <thead class="table-secondary">
                        <th>No.</th>
                        <th>Company Name</th>
                        <th>Company ID</th>
                        <th>Telephone</th>
                        <th>Email</th>
                        <th>Business Purpose</th>
                        <th class="text-center">Screenings</th>
                        <th>Status</th>
                        <th class="text-center">Actions</th>
                    </thead>
                    <tbody>
                    <? if($companies !== false) {
                        $i = 0;
                        foreach($companies as $company) { ?>
                            <tr class="spec-ajax cursor">
                                <td><?= ++$i.'.' ?></td>
                                <td><?= $company['company_name'] ?></td>
                                <td><?= $company['company_id'] ?></td>
                                <td><?= isset($company['telephone'])? $company['telephone'] : 'N/A' ?></td>
                                <td><?= $company['email'] ?></td>
                                <td><?= isset($company['business_purpose'])? $company['business_purpose'] : 'N/A' ?></td>
                                <td class="text-center"><?= $company['screenings'] ?></td>
                                <td><? if ($company['status']==1)  { echo "<span class='text-success'>Approved</span>"; } elseif ($company['status']==-1) { echo "<span class='text-danger'>Disapproved</span>"; } else { echo "<span class='text-warning'>Pending</span>"; } ?></td>
                                <td class="text-center">
                                    <a class="spec-ajax aj-padding-right" href="edit" data-value="<?= $company['company_id'] ?>" data-query="company-edit" data-output=".modal-body" data-dest="<?= __URL__.'/actions/pages.actions.php' ?>" data-toggle="modal" data-title="EDIT COMPANY"><i class="fa fa-edit"></i></a>
                                    <a class="spec-ajax" href="screen" data-value="<?= $company['company_id'] ?>" data-query="company-screen" data-output=".modal-body" data-dest="<?= __URL__.'/actions/pages.actions.php' ?>" data-toggle="modal" data-title="SCREEN COMPANY"><i class="fa fa-lock"></i></a>
                                </td>
                            </tr>
                        <?  } } ?>
                    </tbody>
                </table>
        <?php } ?>
	</div>
</section>
