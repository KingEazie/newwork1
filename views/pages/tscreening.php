<?php
// include controller
include("../../layout/definition.php");
// include transactions screening
include("../../actions/api.actions.php");

$page = new Aejay;

if (isset($_POST['query']) && $_POST['query']=="fetch-page")
    $result = $page->fetch_tranx_screens_pg($_POST['value']);
else
    $result = $page->fetch_tranx_screens();

$url = "https://shop.digitaltermination.com/api/compliance/get-screening-list?page=";    
$current = $result['data'];
$current_page = $current['current_page'];
$last_page = $current['last_page'];
$first_page_url = $current['first_page_url'];
$last_page_url = $current['last_page_url'];
$prev_page_url = $url.($current_page-1);
$next_page_url = $url.($current_page+1);
$from = $current['from'];
$screens = $current['data'];
?>
<section id="tscreening">
    <div class="container-fluid">
        <div class="card container-fluid aj-margin-bottom-20p">
            <h4 class="aj-padding-top-20p text-danger">TRANSACTION SCREENING CHECKLIST</h4>
            <p class="text-muted"><strong class="text-dark">Sources Used: </strong><br>
                Denied Persons List (DPL) - Bureau of Industry and Security, ITAR Debarred (DTC) - State Department, Entity List (EL) - Bureau of Industry and Security, List of Persons Identified as Blocked Solely Pursuant to Executive Order 13599 (13599) - Treasury Department, Foreign Sanctions Evaders (FSE) - Treasury Department, Nonproliferation Sanctions (ISN) - State Department, Part 561 List (561) - Treasury Department, Palestinian Legislative Council List (PLC) - Treasury Department, Specially Designated Nationals (SDN) - Treasury Department, Sectoral Sanctions Identifications List (SSI) - Treasury Department , Unverified List (UVL) - Bureau of Industry and Security
            </p>
        </div>
        <div class="container-fluid aj-margin-bottom-20p">
            <form data-dest="<?= __URL__.'/actions/pages.actions.php' ?>" data-output="#feedback" class="form">
                <div class="row">
                    <div class="form-group col-sm-8">
                        <input type="text" id="item" name="item" required class="form-control" placeholder="Enter search parameter here">
                    </div>
                    <div class="form-group text-center col-sm-4">
                        <input type="hidden" name="tranx-search" value="search">
                        <button class="btn-outline-success form-control" style="width: 50%" type="submit">SEARCH</button>
                    </div>
                </div>
            </form>
        </div>
        <?php if (isset($_POST['query']) && ($_POST['query'] == "t_screening" || $_POST['query'] == "fetch-page") ) { ?>
            <table class="table table-hover table-striped table-responsive-sm">
                <thead class="table-warning">
                    <th>No.</th>
                    <th>MTO</th>
                    <th>Transaction ID</th>
                    <th>Flag Count</th>
                    <th>Flag Results</th>
                    <th>Status</th>
                </thead>
                <tbody>
                <? if($screens !== false) {
                    $i = $from;
                    foreach($screens as $screen) {
                        $data = json_decode($screen['results']);
                     ?>
                        <tr>
                            <td><?= $i++.'.' ?></td>
                            <td><?= empty($screen['mto'])? "NONE" : $screen['mto'] ?></td>
                            <td><?= $screen['transaction_id'] ?></td>
                            <td><?= $data->total ?></td>
                            <td><?= empty($data->results)? "NONE" : implode(", ", $data->results) ?></td>
                            <td><?= ($data->total!=0)? "FLAGGED" : "CLEARED" ?></td>
                       </tr>
                    <?  } } ?>
                </tbody>
            </table>
        <?php } ?>
        <div class="container-fluid aj-padding-top-20p">
            <a href="First Page" class="spec-ajax" data-query="fetch-page" data-value="<?= $first_page_url ?>" data-output="#page-display" data-dest="<?=__URL__.'/views/pages/tscreening.php'?>">First Page</a> |
            <a href="Previous Page" class="spec-ajax" data-query="fetch-page" data-value="<?= ($current_page!=1)? $prev_page_url : $first_page_url ?>" data-output="#page-display" data-dest="<?=__URL__.'/views/pages/tscreening.php'?>">Previous Page</a> |
            <a href="Next Page" class="spec-ajax" data-query="fetch-page" data-value="<?= ($current_page!=$last_page)? $next_page_url : $last_page_url ?>" data-output="#page-display" data-dest="<?=__URL__.'/views/pages/tscreening.php'?>">Next Page</a> |
            <a href="Last Page" class="spec-ajax" data-query="fetch-page" data-value="<?= $last_page_url ?>" data-output="#page-display" data-dest="<?=__URL__.'/views/pages/tscreening.php'?>">Last Page</a>
            <span class="pull-right">Page: <?= $current_page ?></span>
        </div>
    </div>
</section>
