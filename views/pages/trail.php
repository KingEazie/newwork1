<?php
// include controller
include("../../layout/definition.php");
// include transactions
include("../../controllers/pages.controller.php");

$page = new Page;
$trails = $page->fetch_trail();
?>
<section id="trail">
    <div class="container-fluid">
        <?php if (isset($_POST['query']) && $_POST['query'] == "trail") { ?>
            <table class="table table-striped table-responsive-sm">
                <thead class="table-info">
                    <th>Date Time</th>
                    <th>User</th>
                    <th>Activity</th>
                </thead>
                <tbody>
                <? if($trails !== false) {
                    foreach($trails as $trail) { ?>
                        <tr class="cursor">
                            <td class="text-muted"><?= $trail['date_done'] ?></td>
                            <td><?= $trail['username'] ?></td>
                            <td><?= $trail['activity'] ?></td>
                        </tr>
                    <?  } } ?>
                </tbody>
            </table>
        <?php } ?>
    </div>
</section>
