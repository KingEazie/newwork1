<?php  
	// include define
	include("../layout/definition.php");
	// include controller class
	include("../controllers/login.controller.php");
	// initialise controller class
	$class = new Login;

	// LOGIN
	if (isset($_POST['login'])) {
		$credentials = $class->login($_POST['username'],$_POST['passphrase']);
		if ($credentials === false){
			echo '<p>Incorrect Credentials Provided. <br> Please cross-check! and <a href="">Retry</a></p>';	
		}else{
			$_SESSION['user'] = $credentials['first_name'];
			$_SESSION['id'] = $credentials['id'];
            $_SESSION['role'] = $credentials['role'];

            $class->record_audit($_SESSION['id'],"Logged in successfully.");
			echo '<script>window.location.replace("'.__URL__.'/home")</script>';
		}
	}

	// LOGOUT
	if (isset($_POST['query']) && $_POST['query'] == "logout"){
        $class->record_audit($_SESSION['id'],"Logged out successfully.");
		session_destroy();
		echo '<script>window.location="'.__URL__.'/login"</script>';
	}



