<?php  
	//include("../layout/definition.php");

class Aejay {
    protected $func;
    protected $swift_token;
    protected $zp_token;

    function __construct()
    {
        $this->func = new myFunc;
        $this->swift_token = $this->swift_check_token();
        $this->zp_token = $this->zeepay();
    }

    // ZEEPAY CONNECT
    protected function zeepay()
    {
        $url = __HOST__ . '/oauth/token';
        $data =
            "grant_type=" . __GRANT_TYPE__ .
            "&client_id=" . __CLIENT_ID__ .
            "&client_secret=" . __CLIENT_SECRET__ .
            "&username=" . __PHRASE__ .
            "&password=" . __ACCESS__;

        $token = json_decode($this->func->aj_zp_login($url, $data));
        if (isset($token->error))
            $feli = false;
        else {
            //$dated = strtotime($dated);
            //$dated = date("Y-m-d",strtotime("+1 years",$dated));
            $feli = $token->token_type .' '. $token->access_token;
        }
        
        return $feli;
    }
    
    protected function zp_check_token(){
        return isset($this->zp_token) ? $this->zp_token : $this->zeepay();
    }
    
    // SWIFT CONNECT
    protected function swift_login(){
        $url = SWIFT_HOST_.'/oauth2/token';
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_USERPWD => SWIFT_USERNAME_.":".SWIFT_PASSWORD_
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);

        if ($err) {
            echo "cURL Error: ".$err;
            return false;
        } else {
            $response = json_decode($response);
            $this->swift_token = "Bearer ".$response->access_token;
            return $this->swift_token;
        }
    }

    protected function swift_check_token(){
        return isset($this->swift_token) ? $this->swift_token : $this->swift_login();
    }

    private function swift_api_call($url,$data){
        // Initialisation
        $ch = curl_init();
        // Options
        curl_setopt_array($ch, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => $data,
            CURLOPT_HTTPHEADER => array(
                'Authorization: '.$this->swift_token,
                'accept: application/json',
                'cache-control: no-cache',
                'content-type: application/json'
            ),
            //CURLOPT_SSL_VERIFYPEER => false
        ));
        // Execution
        $response = curl_exec($ch);
        $error = curl_error($ch);
        curl_close($ch);
        // Results
        if ($error) {
            echo 'Error: '.$error;
            return 0;
        }else{
            return $response;
        }
    }

    private function swift_put_call($url,$data){
        // Initialisation
        $ch = curl_init();
        // Options
        curl_setopt_array($ch, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'PUT',
            CURLOPT_POSTFIELDS => $data,
            CURLOPT_HTTPHEADER => array(
                'Authorization: '.$this->swift_token,
                'accept: application/json',
                'cache-control: no-cache',
                'content-type: application/json'
            ),
            //CURLOPT_SSL_VERIFYPEER => false
        ));
        // Execution
        $response = curl_exec($ch);
        $error = curl_error($ch);
        curl_close($ch);
        // Results
        if ($error) {
            echo 'Error: '.$error;
            return 0;
        }else{
            return $response;
        }
    }
    

    /*----- ZEEPAY API FUNCTIONALITY -----*/
    
    public function fetch_tranx_screens(){
        $url = __HOST__ .'/api/compliance/get-screening-list';
        return $this->func->aj_zp_get($url,$this->zp_token);
    }  
    
    public function fetch_tranx_screens_pg($url){
        return $this->func->aj_zp_get($url,$this->zp_token);
    }
    
    public function fetch_flag_screens(){
        $url = __HOST__ .'/api/compliance/get-screening-list/flagged-only';
        return $this->func->aj_zp_get($url,$this->zp_token);
    } 
    
    /*----- ZEEPAY API FUNCTIONALITY -----*/
    

    /*----- SWIFT API FUNCTIONALITY -----*/

    public function create_customer($request){
        $url = SWIFT_HOST_.'/customers';
        $request['type']="INDIVIDUAL";

        $data = json_encode($request);
        return $this->swift_api_call($url,$data);
    }

    public function edit_customer($request){
        $id = $request['editindividual'];
        $url = SWIFT_HOST_."/customers/$id";
        $request['type']="INDIVIDUAL";

        $data = json_encode($request);
        return $this->swift_put_call($url,$data);
    }

    public function create_company($request){
        $url = SWIFT_HOST_.'/customers';
        $request['type']="COMPANY";

        $data = json_encode($request);
        return $this->swift_api_call($url,$data);
    }

    public function edit_company($request){
        $id = $request['editcompany'];
        $url = SWIFT_HOST_."/customers/$id";
        $request['type']="COMPANY";

        $data = json_encode($request);
        return $this->swift_put_call($url,$data);
    }

    public function screen_customer($id){
        $url = SWIFT_HOST_."/customers/$id/screenings";
        $data = array("PEP","WATCHLIST","DISQUALIFIED_ENTITIES","ADVERSE_MEDIA");

        $data = json_encode($data);
        return $this->swift_api_call($url,$data);
    }

    public function validate($jsonObj){
        $jobj = json_decode($jsonObj);
        if ($jobj->status=="DONE"){
            $outcomes = $jobj->outcome;
            foreach ($outcomes as $key => $value){
                if ($value == "CLEAR"){
                    $clearlist[] = $key;
                }else{
                    $rejectlist[] = $key;
                }
            }
        }elseif ($jobj->status=="PENDING"){
            $outcomes = $jobj->outcome;
        }

        if (empty($rejectlist))
            return true;
        else
            return $rejectlist;
    }

    /*----- SWIFT API FUNCTIONALITY -----*/

}