<?php  
	include("../layout/definition.php");
	include("../controllers/pages.controller.php");
	include("api.actions.php");

	$page = new Page;
	$aj = new Aejay;

	// define variables
	$info = null;
	$data = array();
	$nationalities = $page->fetch_nationality();

	// create individual
	if (isset($_POST['signindividual'])) {
	    // api call
        foreach ($_POST as $key => $value){
            if (!empty($value) && $value!="aj")
                $data[$key] = $value;
        }
	    $check = json_decode($aj->create_customer($data));

        if ($check->type=='invalid_request') {
            echo '<p class="text-danger text-center">'.$check->errors[0]->message.'</p>';
        } elseif ($check->type=='duplicate' || $check->type=='invalid_token') {
            echo '<p class="text-danger text-center">'.$check->message.'</p>';
        }else {
            $page->record_audit($_SESSION['id'],"Created individual (".$_POST['first_name']." ".$_POST['last_name'].") successfully.");
            $info = $check->id;
            // data entry
            $check = $page->create_individual($info,$_POST['first_name'],$_POST['middle_name'],$_POST['last_name'],$_POST['gender'],$_POST['dob'],$_POST['nationality'],$_POST['mobile'],$_POST['email'],$_SESSION['id']);
            if ($check == "present")
                echo '<p class="text-primary text-center">Individual already created!</p>';
            elseif ($check===true)
                echo '<p class="text-success text-center">Individual successfully created!</p>';
            else
                echo '<p class="text-center">Error! Please Try Again.</p>';
        }
    }

	// edit individual
    if (isset($_POST['editindividual'])) {
        // api call
        foreach ($_POST as $key => $value){
            if (!empty($value))
                $data[$key] = $value;
        }
        $check = json_decode($aj->edit_customer($data));

        if ($check->type=='invalid_request') {
            echo '<p class="text-danger text-center">'.$check->errors[0]->message.'</p>';
        } elseif ($check->type=='invalid_token') {
            echo '<p class="text-danger text-center">'.$check->message.'</p>';
        }else {
            $page->record_audit($_SESSION['id'],"Edited individual (".$_POST['first_name']." ".$_POST['first_name'].") successfully.");
            $dob = isset($_POST['dob'])? null:$_POST['dob'];
            // edit entry
            $check = $page->edit_individual($_POST['editindividual'],$_POST['gender'],$dob,$_POST['nationality'],$_POST['mobile'],$_POST['email']);
            if ($check===true)
                echo '<p class="text-success text-center">Individual successfully edited!</p>';
            else
                echo '<p class="text-center">Error! Please Try Again.</p>';
        }
    }

    // create company
    if (isset($_POST['signcompany'])) {
        // api call
        foreach ($_POST as $key => $value){
            if (!empty($value) && $value!="aj")
                $data[$key] = $value;
        }
        $check = json_decode($aj->create_company($data));

        if ($check->type=='invalid_request') {
            echo '<p class="text-danger text-center">'.$check->errors[0]->message.'</p>';
        } elseif ($check->type=='duplicate' || $check->type=='invalid_token') {
            echo '<p class="text-danger text-center">'.$check->message.'</p>';
        }else {
            $page->record_audit($_SESSION['id'],"Created company (".$_POST['company_name'].") successfully.");
            $info = $check->id;
            // data entry
            $check = $page->create_company($info,$_POST['company_name'],$_POST['telephone'],$_POST['email'],$_POST['business_purpose'],$_POST['incorporation_number'],$_POST['incorporation_type'],$_POST['incorporation_country'],$_SESSION['id']);
            if ($check=="present")
                echo '<p class="text-primary text-center">Company already created!</p>';
            elseif ($check===true)
                echo '<p class="text-success text-center">Company successfully created!</p>';
            else
                echo '<p class="text-center">Error! Please Try Again.</p>';
        }
    }

    // edit company
    if (isset($_POST['editcompany'])) {
        // api call
        foreach ($_POST as $key => $value) {
            if (!empty($value))
                $data[$key] = $value;
        }
        $check = json_decode($aj->edit_company($data));

        if ($check->type == 'invalid_request') {
            echo '<p class="text-danger text-center">' . $check->errors[0]->message . '</p>';
        } elseif ($check->type == 'invalid_token') {
            echo '<p class="text-danger text-center">' . $check->message . '</p>';
        } elseif ($check->type == 'resource_not_found') {
            echo '<p class="text-warning text-center">' . $check->message . '</p>';
            // edit entry
            $check = $page->edit_company($_POST['editcompany'], $_POST['telephone'], $_POST['email'], $_POST['business_purpose'], $_POST['incorporation_number'], $_POST['incorporation_type'], $_POST['incorporation_country']);
        } else {
            // edit entry
            $check = $page->edit_company($_POST['editcompany'], $_POST['telephone'], $_POST['email'], $_POST['business_purpose'], $_POST['incorporation_number'], $_POST['incorporation_type'], $_POST['incorporation_country']);
        }

        if ($check === true) {
            $page->record_audit($_SESSION['id'], "Edited company (" . $_POST['company_name'] . ") successfully.");
            echo '<p class="text-success text-center">Company successfully edited!</p>';
        }else
            echo '<p class="text-center">Error! Please Try Again.</p>';
    }

    // Screening Results
    if (isset($_POST['query']) && $_POST['query'] == "individual-screen") {
        $info = $page->fetch_individual($_POST['value']); ?>
        <div class="container-fluid aj-padding-bottom-20p">
            <small class="text-muted">ID</small><br>
            <p><?= $info['individual_id'] ?></p>
            <small class="text-muted">Full Name</small><br>
            <p><?= $info['first_name'].' '.$info['middle_name'].' '.$info['last_name'] ?></p>
            <small class="text-muted">Status</small><br>
            <? if ($info['status']==1)  { echo "<span class='text-success'>Approved</span>"; } elseif ($info['status']==-1) { echo "<span class='text-danger'>Disapproved</span>"; } else { echo "<span class='text-warning'>Pending</span>"; } ?>
        </div>
        <button class="btn btn-outline-success spec-ajax" data-dest="<?= __URL__.'/actions/pages.actions.php' ?>" data-query="individual-confirm-screen" id="<?= $info['id'] ?>" data-value="<?= $info['individual_id'] ?>" data-output=".modal-body">SCREEN</button>
<? } elseif (isset($_POST['query']) && $_POST['query'] == "company-screen") {
        $info = $page->fetch_company($_POST['value']); ?>
        <div class="container-fluid aj-padding-bottom-20p">
            <small class="text-muted">ID</small><br>
            <p><?= $info['company_id'] ?></p>
            <small class="text-muted">Company Name</small><br>
            <p><?= $info['company_name'] ?></p>
            <small class="text-muted">Status</small><br>
            <? if ($info['status']==1)  { echo "<span class='text-success'>Approved</span>"; } elseif ($info['status']==-1) { echo "<span class='text-danger'>Disapproved</span>"; } else { echo "<span class='text-warning'>Pending</span>"; } ?>
        </div>
        <button class="btn btn-outline-success spec-ajax" data-dest="<?= __URL__.'/actions/pages.actions.php' ?>" data-query="company-confirm-screen" id="<?= $info['id'] ?>" data-value="<?= $info['company_id'] ?>" data-output=".modal-body">SCREEN</button>
<? }

    // Screening Confirmation
    if (isset($_POST['query']) && $_POST['query'] == "individual-confirm-screen") {
        $id = $_POST['id'];
        $value = $_POST['value'];
        $check = json_decode($aj->screen_customer($value));
        if (isset($check->type) && ($check->type=='invalid_token' || $check->type=='internal_error')) {
            echo '<p class="text-danger text-center">Error! Please Try Again.</p>';
        } elseif (isset($check->type) && $check->type=='service_maintenance') {
            echo '<p class="text-warning text-center">Schedule Maintenance! Contact Technical</p>';
        } else {
            $page->record_audit($_SESSION['id'],"Screened Customer (".$value.") successfully.");
            $page->individual_add_screen($id);
            $outcomes = $check->outcome;  ?>
            <div class="container-fluid aj-padding-bottom-20p">
                <small class="text-muted">Full Name</small><br>
                <p><?= $check->entity_name ?></p>
                <small class="text-muted">Status</small><br>
                <p><?= $check->status ?></p>
                <small class="text-muted">Outcome</small><br>
                <p><? foreach ($outcomes as $key => $value) { echo $key.' : '.$value.'<br>'; } ?></p>
            </div>
            <form data-dest="<?php echo __URL__.'/actions/pages.actions.php' ?>" data-output="#feedback" class="form">
                <div class="row">
                    <div class="form-group col-sm-8">
                        <select id="status" name="status" class="form-control" required>
                            <optgroup label="Status">
                                <option>Decide Later</option>
                                <option value="1">Approve Individual</option>
                                <option value="-1">Disapprove Individual</option>
                            </optgroup>
                        </select>
                    </div>
                    <div class="form-group col-sm-4">
                        <input type="hidden" name="individual-yes-id" value="<?= $check->customer_id ?>">
                        <input type="hidden" name="individual-screen-id" value="<?= $check->id ?>">
                        <input type="hidden" name="individual-status" value="<?= $check->status ?>">
                        <input type="hidden" name="individual-outcomes" value='<?= json_encode($outcomes) ?>'>
                        <input type="hidden" name="individual-match" value="<?= isset($check->match_id)? $check->match_id:null ?>">
                        <input class="form-control btn-outline-success" type="submit" value="Confirm">
                    </div>
                </div>
                <div id="feedback" class="wrap"></div>
            </form>
        <? } ?>
<?  } elseif (isset($_POST['query']) && $_POST['query'] == "company-confirm-screen") {
        $id = $_POST['id'];
        $value = $_POST['value'];
        $check = json_decode($aj->screen_customer($value));
        if (isset($check->type) && ($check->type == 'invalid_token' || $check->type == 'internal_error')) {
            echo '<p class="text-danger text-center">Error! Please Try Again.</p>';
        } elseif (isset($check->type) && $check->type == 'service_maintenance') {
            echo '<p class="text-warning text-center">Schedule Maintenance! Contact Technical</p>';
        } elseif (isset($check->type) && $check->type == "resource_not_found") {
            $page->record_audit($_SESSION['id'],"Screened Company (".$value.") successfully.");
            $page->company_add_screen($id);
            echo '<p class="text-warning text-center">No details found on company!</p>'; ?>
            <form data-dest="<?php echo __URL__.'/actions/pages.actions.php' ?>" data-output="#feedback" class="form">
                <div class="row">
                    <div class="form-group col-sm-8">
                        <select id="status" name="status" class="form-control" required>
                            <optgroup label="Status">
                                <option>Decide Later</option>
                                <option value="1">Approve Company</option>
                                <option value="-1">Disapprove Company</option>
                            </optgroup>
                        </select>
                    </div>
                    <div class="form-group col-sm-4">
                        <input type="hidden" name="company-yes-id" value="<?= $value ?>">
                        <input type="hidden" name="company-screen-id" value="<?= $check->id ?>">
                        <input type="hidden" name="company-status" value="NOT FOUND">
                        <input type="hidden" name="company-outcomes" value="NO RESOURCE">
                        <input type="hidden" name="company-match" value="">
                        <input class="form-control btn-outline-success" type="submit" value="Confirm">
                    </div>
                </div>
                <div id="feedback" class="wrap"></div>
            </form>
<?    } elseif (isset($check->outcome) && isset($check->status)) {
            $page->record_audit($_SESSION['id'],"Screened Company (".$value.") successfully.");
            $page->company_add_screen($id);
            $outcomes = $check->outcome;  ?>
            <div class="container-fluid aj-padding-bottom-20p">
                <small class="text-muted">Company Name</small><br>
                <p><?= $check->entity_name ?></p>
                <small class="text-muted">Status</small><br>
                <p><?= $check->status ?></p>
                <small class="text-muted">Outcome</small><br>
                <p><? foreach ($outcomes as $key => $value) { echo $key.' : '.$value.'<br>'; } ?></p>
            </div>
            <form data-dest="<?php echo __URL__.'/actions/pages.actions.php' ?>" data-output="#feedback" class="form">
                <div class="row">
                    <div class="form-group col-sm-8">
                        <select id="status" name="status" class="form-control" required>
                            <optgroup label="Status">
                                <option>Decide Later</option>
                                <option value="1">Approve Company</option>
                                <option value="-1">Disapprove Company</option>
                            </optgroup>
                        </select>
                    </div>
                    <div class="form-group col-sm-4">
                        <input type="hidden" name="company-yes-id" value="<?= $check->customer_id ?>">
                        <input type="hidden" name="company-screen-id" value="<?= $check->id ?>">
                        <input type="hidden" name="company-status" value="<?= $check->status ?>">
                        <input type="hidden" name="company-outcomes" value='<?= json_encode($outcomes) ?>'>
                        <input type="hidden" name="company-match" value="<?= isset($check->match_id)? $check->match_id:null ?>">
                        <input class="form-control btn-outline-success" type="submit" value="Confirm">
                    </div>
                </div>
                <div id="feedback" class="wrap"></div>
            </form>
<?php   } else {
            echo '<p class="text-danger text-center">$check->message</p>';
        }

    }

    // Screening Conclusion
    if (isset($_POST['individual-yes-id'])) {
        $check = $page->approve_individual($_POST['individual-yes-id'], $_POST['individual-screen-id'], $_POST['individual-status'], $_POST['individual-outcomes'], $_POST['individual-match'], $_POST['status'], $_SESSION['id']);
        if ($check) {
            $page->record_audit($_SESSION['id'],"Approval for Individual (".$_POST['individual-yes-id'].") set successfully.");
            echo '<p class="text-success text-center">Individual Approval Set!</p>';
        }else
            echo '<p class="text-danger text-center">Error! Please Try Again.</p>';
    } elseif (isset($_POST['company-yes-id'])) {
        $check =  $page->approve_company($_POST['company-yes-id'],$_POST['company-screen-id'],$_POST['company-status'],$_POST['company-outcomes'],$_POST['company-match'],$_POST['status'],$_SESSION['id']);
        if ($check) {
            $page->record_audit($_SESSION['id'], "Approval for Company (" . $_POST['company-yes-id'] . ") set successfully.");
            echo '<p class="text-success text-center">Company Approval Set!</p>';
        }else
            echo '<p class="text-danger text-center">Error! Please Try Again.</p>';
    }

    // Screening Re-address
    if (isset($_POST['query']) && $_POST['query'] == "individual-entity") {
        $id = $_POST['id'];
        $info = $page->fetch_individual($_POST['value']); ?>
        <div class="container-fluid aj-padding-bottom-20p">
            <small class="text-muted">ID</small><br>
            <p><?= $info['individual_id'] ?></p>
            <small class="text-muted">Full Name</small><br>
            <p><?= $info['first_name'].' '.$info['middle_name'].' '.$info['last_name'] ?></p>
            <small class="text-muted">Status</small><br>
            <? if ($info['status']==1)  { echo "<span class='text-success'>Approved</span>"; } elseif ($info['status']==-1) { echo "<span class='text-danger'>Disapproved</span>"; } else { echo "<span class='text-warning'>Pending</span>"; } ?>
        </div>
        <form data-dest="<?php echo __URL__.'/actions/pages.actions.php' ?>" data-output="#feedback" class="form">
            <div class="row col">
                <div class="form-group col-sm-8">
                    <select id="status" name="status" class="form-control" required>
                        <optgroup label="Status">
                            <option>Decide Later</option>
                            <option value="1">Approve Individual</option>
                            <option value="-1">Disapprove Individual</option>
                        </optgroup>
                    </select>
                </div>
                <div class="form-group col-sm-4">
                    <input type="hidden" name="individual-redress" value="<?= $info['individual_id'] ?>">
                    <input type="hidden" name="screen-id" value="<?= $id ?>">
                    <input class="form-control btn-outline-success" type="submit" value="Confirm">
                </div>
            </div>
            <div id="feedback" class="wrap"></div>
        </form>
<?  } elseif (isset($_POST['query']) && $_POST['query'] == "company-entity") {
        $id = $_POST['id'];
        $info = $page->fetch_company($_POST['value']); ?>
        <div class="container-fluid aj-padding-bottom-20p">
            <small class="text-muted">ID</small><br>
            <p><?= $info['company_id'] ?></p>
            <small class="text-muted">Company Name</small><br>
            <p><?= $info['company_name'] ?></p>
            <small class="text-muted">Status</small><br>
            <? if ($info['status']==1)  { echo "<span class='text-success'>Approved</span>"; } elseif ($info['status']==-1) { echo "<span class='text-danger'>Disapproved</span>"; } else { echo "<span class='text-warning'>Pending</span>"; } ?>
        </div>
        <form data-dest="<?php echo __URL__.'/actions/pages.actions.php' ?>" data-output="#feedback" class="form">
            <div class="row col">
                <div class="form-group col-sm-8">
                    <select id="status" name="status" class="form-control" required>
                        <optgroup label="Status">
                            <option>Decide Later</option>
                            <option value="1">Approve Company</option>
                            <option value="-1">Disapprove Company</option>
                        </optgroup>
                    </select>
                </div>
                <div class="form-group col-sm-4">
                    <input type="hidden" name="company-redress" value="<?= $info['company_id'] ?>">
                    <input type="hidden" name="screen-id" value="<?= $id ?>">
                    <input class="form-control btn-outline-success" type="submit" value="Confirm">
                </div>
            </div>
            <div id="feedback" class="wrap"></div>
        </form>
<? }

    // Set Status
    if (isset($_POST['individual-redress'])) {
        $check = $page->individual_set_status($_POST['status'], $_POST['individual-redress']);
        if ($check) {
            if (!empty($_POST['status']))
                $page->update_screen($_POST['status'],$_POST['screen-id']);
            echo '<p class="text-success text-center">Individual Approval Set!</p>';
        }else
            echo '<p class="text-danger text-center">Error! Please Try Again.</p>';
    } elseif (isset($_POST['company-redress'])) {
        $check = $page->company_set_status($_POST['status'], $_POST['company-redress']);
        if ($check) {
            if (!empty($_POST['status']))
                $page->update_screen($_POST['status'],$_POST['screen-id']);
            echo '<p class="text-success text-center">Company Approval Set!</p>';
        }else
            echo '<p class="text-danger text-center">Error! Please Try Again.</p>';
    }

    // Update Entities
    if (isset($_POST['query']) && $_POST['query'] == "individual-edit") {
        $info = $page->fetch_individual($_POST['value']); ?>
        <form data-dest="<?php echo __URL__.'/actions/pages.actions.php' ?>" data-output=".modal-body" class="form" id="individualform">
            <div class="form-group">
                <label for="first_name">First Name</label>
                <input class="form-control validate" type="text" id="first_name" name="first_name" value="<?= $info['first_name'] ?>" required>
            </div>
            <div class="form-group">
                <label for="middle_name">Middle Name</label>
                <input class="form-control validate" type="text" id="middle_name" name="middle_name" value="<?= $info['middle_name'] ?>">
            </div>
            <div class="form-group">
                <label for="last_name">Last Name</label>
                <input class="form-control validate" type="text" id="last_name" name="last_name" value="<?= $info['last_name'] ?>" required>
            </div>
            <div class="row">
                <div class="form-group col-sm-4">
                    <label for="gender">Gender</label>
                    <select id="gender" name="gender" class="form-control">
                        <optgroup label="Gender">
                            <option value="">Unknown</option>
                            <option value="MALE" selected="<?= ($info['gender']=="MALE")? true:false ?>">Male</option>
                            <option value="FEMALE" selected="<?= ($info['gender']=="FEMALE")? true:false ?>">Female</option>
                        </optgroup>
                    </select>
                </div>
                <div class="form-group col-sm-8">
                    <label for="dob">DOB</label>
                    <? if (!empty($info['dob'])) { ?>
                        <input class="form-control validate" type="text" id="dob" name="dob" value="<?= date("d/m/Y", strtotime($info['dob'])) ?>">
                    <? }else{ ?>
                        <input class="form-control validate" type="date" id="dob" name="dob">
                    <? } ?>
                </div>
            </div>
            <div class="form-group">
                <label for="nationality">Nationality</label>
                <select id="nationality" name="nationality" class="form-control" required>
                    <optgroup label="Nationality">
                        <? foreach ($nationalities as $nationality) { ?>
                            <option value="<?= $nationality['id'] ?>" <?= ($info['nationality']==$nationality['id'])? 'selected':'' ?> ><?= $nationality['name'] ?></option>
                        <? } ?>
                    </optgroup>
                </select>
            </div>
            <div class="form-group">
                <label for="mobile">Mobile</label>
                <input class="form-control validate" type="text" id="mobile" name="mobile" value="<?= $info['mobile'] ?>" placeholder="Eg. +233 xx xxx xxxx">
            </div>
            <div class="form-group">
                <label for="email">Email</label>
                <input class="form-control validate" type="email" id="email" name="email" value="<?= $info['email'] ?>" required>
            </div>
            <div class="form-group center-align">
                <input class="form-control" type="hidden" name="editindividual" value="<?= $info['individual_id'] ?>">
                <button type="submit" class="form-control btn btn-outline-primary">EDIT INDIVIDUAL</button>
            </div>
        </form>
<?  } elseif (isset($_POST['query']) && $_POST['query'] == "company-edit") {
        $info = $page->fetch_company($_POST['value']); ?>
        <form data-dest="<?php echo __URL__.'/actions/pages.actions.php' ?>" data-output=".modal-body" class="form">
            <div class="form-group">
                <label for="company_name">Company Name</label>
                <input class="form-control validate" type="text" id="company_name" name="company_name" value="<?= $info['company_name'] ?>" required>
            </div>
            <div class="form-group">
                <label for="email">Email</label>
                <input class="form-control validate" type="email" id="email" name="email" value="<?= $info['email'] ?>" required>
            </div>
            <div class="form-group">
                <label for="telephone">Telephone</label>
                <input class="form-control validate" type="text" id="telephone" name="telephone" value="<?= $info['telephone'] ?>" placeholder="Eg. +233 xx xxx xxxx">
            </div>
            <div class="form-group">
                <label for="incorporation_number">Incorporation Number</label>
                <input class="form-control validate" type="text" id="incorporation_number" name="incorporation_number" value="<?= $info['incorporation_number'] ?>">
            </div>
            <div class="form-group">
                <label for="incorporation_type">Incorporation Type</label>
                <select id="incorporation_type" name="incorporation_type" class="form-control">
                    <optgroup label="Incorporation Type">
                        <option value="">Unknown</option>
                        <option value="SOLE_TRADER">Sole Trader</option>
                        <option value="PRIVATE_LIMITED">Private Limited</option>
                        <option value="LIMITED_LIABILITY_PARTNERSHIP">Limited Liability Partnership</option>
                        <option value="PUBLIC_LIMITED">Public Limited</option>
                    </optgroup>
                </select>
            </div>
            <div class="form-group">
                <label for="incorporation_country">Incorporation Country</label>
                <select id="incorporation_country" name="incorporation_country" class="form-control" required>
                    <optgroup label="Incorporation Country">
                        <? foreach ($nationalities as $nationality) { ?>
                            <option value="<?= $nationality['id'] ?>" <?= ($info['incorporation_country']==$nationality['id'])? 'selected':'' ?> ><?= $nationality['name'] ?></option>
                        <? } ?>
                    </optgroup>
                </select>
            </div>
            <div class="form-group">
                <label for="business_purpose">Business Purpose</label>
                <select id="business_purpose" name="business_purpose" class="form-control">
                    <optgroup label="Business Purpose">
                        <option value="" >Unknown</option>
                        <option value="REGULATED_ENTITY" <?= ($info['business_purpose']=="REGULATED_ENTITY")? 'selected':'' ?> >Regulated Entity</option>
                        <option value="PRIVATE_ENTITY" <?= ($info['business_purpose']=="PRIVATE_ENTITY")? 'selected':'' ?> >Private Entity</option>
                        <option value="UNREGULATED_FUND" <?= ($info['business_purpose']=="UNREGULATED_FUND")? 'selected':'' ?> >Unregulated Fund</option>
                        <option value="TRUST" <?= ($info['business_purpose']=="TRUST")? 'selected':'' ?> >Trust</option>
                        <option value="FOUNDATION" <?= ($info['business_purpose']=="FOUNDATION")? 'selected':'' ?> >Foundation</option>
                        <option value="RELIGIOUS_BODY" <?= ($info['business_purpose']=="RELIGIOUS_BODY")? 'selected':'' ?> >Religious Body</option>
                        <option value="GOVERNMENT_ENTITY" <?= ($info['business_purpose']=="GOVERNMENT_ENTITY")? 'selected':'' ?> >Government Entity</option>
                        <option value="CHARITY" <?= ($info['business_purpose']=="CHARITY")? 'selected':'' ?> >Charity</option>
                        <option value="CLUB" <?= ($info['business_purpose']=="CLUB")? 'selected':'' ?> >Club</option>
                        <option value="SOCIETY" <?= ($info['business_purpose']=="SOCIETY")? 'selected':'' ?> >Society</option>
                    </optgroup>
                </select>
            </div>
            <div class="form-group center-align">
                <input class="form-control" type="hidden" name="editcompany" value="<?= $info['company_id'] ?>">
                <button type="submit" class="form-control btn btn-outline-success">EDIT COMPANY</button>
            </div>
        </form>
<?php }