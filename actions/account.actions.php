<?php  
	// include defines
	include("../layout/definition.php");
	// include controller
	include("../controllers/account.controller.php");
	// initialize class
	$class = new Account;

    // ACCOUNT DETAILS CREATE
    if (isset($_POST['user-confirm'])) {
        $result = $class->add_user($_POST['first_name'],$_POST['last_name'],$_POST['username'],$_POST['email'],$_POST['role'],$_POST['status'],$_SESSION['id']);

        // check if correct
        if ($result === true){
            $class->record_audit($_SESSION['id'],"User(".$_POST['username'].") account created successfully.");
            echo '<p class="text-success text-center">User created successfully!</p>';
        }else
            echo '<p class="text-danger text-center">An unexpected error occurred! Try Again.</p>';
    }

	// ACCOUNT DETAILS UPDATE
	if (isset($_POST['editaccount'])) {
		$result = $class->update($_POST['first_name'],$_POST['last_name'],$_POST['username'],$_POST['email'],$_POST['password'],$_SESSION['id']);

		// check if correct
		if ($result === true){
            $class->record_audit($_SESSION['id'],"Edited account successfully.");
			$_SESSION['user'] = $_POST['username'];
            echo '<p class="text-success text-center">Updated successfully!</p>';
		}else
			echo '<p class="text-danger text-center">An unexpected error occurred! Try Again.</p>';
	}

	// PASSWORD CHANGE
	if (isset($_POST['passchange'])) {
        if ($_POST['npass'] != $_POST['rpass']) {
            echo '<p class="text-danger text-center">Passwords don\'t match</p>';
            return;
        }

        $result = $class->pass($_POST['opass'], $_POST['npass'], $_SESSION['id']);

        if ($result === true) {
            $class->record_audit($_SESSION['id'], "Changed Password successfully.");
            echo '<p class="text-success text-center">Password changed Successfully</p>';
        }else
			echo '<p class="text-danger text-center">An unexpected error occurred.</p>';
	}

    if (isset($_POST['query']) && $_POST['query'] == "reset-user") { ?>
        <h4 class="text-muted">Do you wish to reset user account?</h4>
        <button class="btn btn-outline-success spec-ajax" data-value="<?= $_POST['value']?>" data-query="confirm-reset-user" data-output=".modal-body" data-dest="<?= __URL__.'/actions/account.actions.php' ?>">CONFIRM</button>

    <?   } elseif (isset($_POST['query']) && $_POST['query'] == "confirm-reset-user") {
        if ($class->reset_user($_POST['value']))
            echo '<p class="text-success text-center">User Password successfully reset to Default</p>';
        else
            echo '<p class="text-danger text-center">An Error Occurred! Please Try Again.</p>';

    } elseif (isset($_POST['query']) && $_POST['query'] == "edit-user") {
        $info = $class->fetch_user($_POST['id']);
        ?>
        <form class="form" data-dest="<?php echo __URL__.'/actions/account.actions.php' ?>" data-output=".modal-body">
            <div class="form-group">
                <label for="first_name">First Name</label>
                <input class="form-control validate" type="text" id="first_name" name="first_name" value="<?= $info['first_name'] ?>" required>
            </div>
            <div class="form-group">
                <label for="last_name">Last Name</label>
                <input class="form-control validate" type="text" id="last_name" name="last_name" value="<?= $info['last_name'] ?>" required>
            </div>
            <div class="form-group">
                <label for="username">Username</label>
                <input class="form-control validate" type="text" id="username" name="username" value="<?= $info['username'] ?>" required>
            </div>
            <div class="form-group">
                <label for="email">Email</label>
                <input class="form-control validate" type="email" id="email" name="email" value="<?= $info['email'] ?>" required>
            </div>
            <div class="form-group">
                <select class="form-control" required name="role-selected">
                    <optgroup label="set Role">
                        <option value="1" <?= ($info['role'] == 1)? 'selected' : '' ?> >Restricted</option>
                        <option value="3" <?= ($info['role'] == 3)? 'selected' : '' ?> >Administration</option>
                        <option value="5" <?= ($info['role'] == 5)? 'selected' : '' ?> >Management</option>
                        <option value="7" <?= ($info['role'] == 7)? 'selected' : '' ?> >Senior Management</option>
                        <option value="9" <?= ($info['role'] == 9)? 'selected' : '' ?> >Administrator</option>
                        <option value="10" <?= ($info['role'] == 10)? 'selected' : '' ?> >Technical</option>
                    </optgroup>
                </select>
            </div>
            <div class="form-group">
                <select class="form-control" required name="status-selected">
                    <optgroup label="set status">
                        <option value="1" <?= ($info['status'] == 1)? 'selected' : '' ?> >Active</option>
                        <option value="0" <?= ($info['status'] == 0)? 'selected' : '' ?> >Inactive</option>
                        <option value="-1" <?= ($info['status'] == -1)? 'selected' : '' ?> >Suspend</option>
                    </optgroup>
                </select>
            </div>
            <div class="form-group">
                <input type="hidden" name="user-status-confirm" value="<?= $_POST['id']?>">
                <button type="submit" class="btn btn-outline-success form-control">Confirm</button>
            </div>
        </form>
    <?   } elseif (isset($_POST['user-status-confirm'])) {
        if ($class->update_status_user($_POST['first_name'], $_POST['last_name'], $_POST['username'], $_POST['email'], $_POST['role-selected'], $_POST['status-selected'], $_POST['user-status-confirm'])) {
            $class->record_audit($_SESSION['id'],"updated user account (".$_POST['username'].") successfully.");
            echo '<p class="text-success text-center">User updated successfully.</p>';
        }else
            echo '<p class="text-danger text-center">An Error Occurred! Please Try Again.</p>';
    }
