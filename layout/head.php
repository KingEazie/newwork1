<head>
  <title>Compliance Portal</title>
  <!--Import Google Icons Fonts-->
  <link href="<?php echo __ASSETS__.'/css/font-awesome.min.css' ?>" rel="stylesheet">

  <!--Import bootstrap.css-->
  <link type="text/css" rel="stylesheet" href="<?php echo __ASSETS__.'/css/bootstrap.min.css' ?>"/>

  <link rel="stylesheet" href="<?php echo __ASSETS__.'/css/styles.css' ?>"/>

  <!--Let browser know website is optimized for mobile-->
  <meta name="viewport" content="initial-scale=1.0, user-scalable=no"/>
  <meta charset="utf-8"/>

  <!--Favicon-->
  <link rel="icon" type="image/png" href="<?php echo __ASSETS__.'/img/favicon.png' ?>"/>

   <!--Import jQuery before bootstrap.js-->
  <script type="text/javascript" src="<?php echo __ASSETS__.'/js/jquery-3.2.0.min.js' ?>"></script>
  <script type="text/javascript" src="<?php echo __ASSETS__.'/js/bootstrap.bundle.min.js' ?>"></script>
  <script type="text/javascript" src="<?php echo __ASSETS__.'/js/index.js' ?>"></script>
</head>

<body>
