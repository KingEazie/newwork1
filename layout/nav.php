<?php 
  if(!isset($_SESSION['id'])){
    header("Location: ".__URL__."/login");
  }else{
      include("../../controllers/home.controller.php");
      // include transactions api action
      include("../../actions/api.actions.php");

      $page = new Aejay;
      $aj = new Home;

      list($individuals,$companies,$screening,$pending,$tasks) = $aj->index();
    
      $tresult = $page->fetch_tranx_screens();
      $tcurrent = $tresult['data'];
      $total_tranx = $tcurrent['total'];
      
      $fresult = $page->fetch_flag_screens();
      $fcurrent = $fresult['data'];
      $flagged_tranx = $fcurrent['total'];

      date_default_timezone_set("Africa/Accra");
      $date = date("Y");
      
?>

<header>
    <nav class="navbar navbar-expand-sm bg-secondary navbar-dark fixed-top ">
        <ul class="navbar-nav">
            <li class="nav-item">
                <a href="#menu-toggle" class="btn btn-outline-danger" id="menu-toggle"><i class="fa fa-bars fa-2x"></i></a>
            </li>
        </ul>
        <!-- Brand -->
        <a class="navbar-brand text-dark" href="" style="padding-left: 1%"><img src="<?= __ASSETS__.'/img/Zeepay_Logo.png' ?>"></a>

        <div class="collapse navbar-collapse justify-content-center">
            <h1 id="brand" class="text-white-50">Compliance Portal</h1>
        </div>
        <!-- Navbar links -->
        <div class="collapse navbar-collapse justify-content-end" id="collapsibleNavbar">
            <ul class="navbar-nav">
                <!-- Dropdown -->
                <li class="nav-item cursor aj-padding-right <?= ($_SESSION['role']<5)? 'd-none':''?>" style="padding-top: 9px">
                    <a href="#" class="text-white" data-toggle="popover" data-trigger="focus" data-placement="bottom" title="Notifications"
                       data-content="<? if($tasks !== false) { foreach ($tasks as $task){echo $task['id']; } } else { echo 'You have No new notifications!'; } ?>">
                        <i class="fa fa-tasks"></i>
                        <span class="badge badge-pill badge-danger"><?= ($tasks != false)? count($tasks):"" ?></span>
                    </a>
                </li>
                <li class="nav-item dropdown sidebar-brand">
                    <a class="nav-link dropdown-toggle text-white" href="#" id="navbardrop" data-toggle="dropdown">
                        <span><i class="fa fa-user"></i> <span><?= $_SESSION['user'] ?> </span></span>
                    </a>
                    <div class="dropdown-menu">
                        <a class="dropdown-item spec-ajax" href="account" data-query="account" data-output=".modal-body" data-dest="<?= __URL__.'/views/home/account.php' ?>" data-toggle="modal" data-title="PROFILE">account</a>
                        <a class="dropdown-item spec-ajax" href="logout" data-query="logout" data-output=".modal-dialog" data-dest="<?= __URL__.'/actions/login.actions.php' ?>" data-toggle="modal">logout</a>
                    </div>
                </li>
            </ul>
        </div>
    </nav>
</header>
<section>

</section>
<div id="wrapper">
    <!-- Sidebar -->
    <div id="sidebar-wrapper">
        <ul class="sidebar-nav aj-padding-top-35">
            <li class="active-menu">
                <a href="<?= __URL__.'/home' ?>"><i class="fa fa-dashboard aj-padding-right"></i> Dashboard</a>
            </li>
            <li>
                <a href="#services" data-toggle="collapse"><i class="fa fa-list-alt aj-padding-right"></i> Entities</a>
                <ul id="services" class="collapse no-list-symbols">
                    <li class="menu">
                        <a href="individual" class="spec-ajax" data-query="individual" data-output="#page-display" data-dest="<?=__URL__.'/views/pages/entity.php'?>"><i class="fa fa-users aj-padding-right" style="padding-right: 20px"></i> Individual</a>
                    </li>
                    <li class="menu">
                        <a href="company" class="spec-ajax" data-query="company" data-output="#page-display" data-dest="<?=__URL__.'/views/pages/entity.php'?>"><i class="fa fa-building aj-padding-right"></i> Company</a>
                    </li>
                </ul>
            </li>
            <li class="menu">
                <a href="screenings" class="spec-ajax" data-query="screenings" data-output="#page-display" data-dest="<?=__URL__.'/views/pages/screenings.php'?>"><i class="fa fa-shield aj-padding-right"></i> Entity Screening</a>
            </li>
            <li class="menu">
                <a href="t_screening" class="spec-ajax" data-query="t_screening" data-output="#page-display" data-dest="<?=__URL__.'/views/pages/tscreening.php'?>"><i class="fa fa-list aj-padding-right"></i> Tranx Screening</a>
            </li>
            <li class="menu">
                <a href="f_screening" class="spec-ajax <?= ($_SESSION['role']<9)? 'd-none':''?>" data-query="f_screening" data-output="#page-display" data-dest="<?=__URL__.'/views/pages/fscreening.php'?>"><i class="fa fa-list aj-padding-right"></i> Flag Screening</a>
            </li>
            <li class="menu">
                <a href="users" class="spec-ajax <?= ($_SESSION['role']<7)? 'd-none':''?>" data-query="users" data-output="#page-display" data-dest="<?=__URL__.'/views/home/account.php'?>"><i class="fa fa-user-secret aj-padding-right"></i> Users</a>
            </li>
            <li class="menu">
                <a href="trail" class="spec-ajax <?= ($_SESSION['role']<9)? 'd-none':''?>" data-query="trail" data-output="#page-display" data-dest="<?=__URL__.'/views/pages/trail.php'?>"><i class="fa fa-sliders aj-padding-right"></i> Audit Trail</a>
            </li>
            <li class="dropdown-divider"></li>
            <li class="text-center">
                <a class="text-white-50">&copy <?= $date ?></a>
            </li>
        </ul>
    </div>
    <!-- /#sidebar-wrapper -->

    <!-- Page Content -->
    <div id="page-content-body" class="space">
        <section id="pages-content-wrapper" class="aj-padding-top-20p">
            <div id="page-display" class="container-fluid">
                <? include("../../views/home/home.php"); ?>
            </div>
        </section>
    </div>
    <!-- /#page-content-wrapper -->
</div>
<!-- /#wrapper -->

<!-- Menu Toggle Script -->
<script>
    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });
</script>
<?php } ?>