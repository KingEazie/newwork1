<?php  
	/*
	*/
	class Account
	{
		protected $func;

		function __construct()
		{
			$this->func = new myFunc;
		}


	// ACCOUNT DETAILS FETCH
		public function index($id){
			$account = $this->func->myQuery("SELECT * FROM users WHERE id = ?","i",array($id),"fetch");
			return $account;
		}


	// ACCOUNT DETAILS UPDATE
		public function update($fname,$lname,$uname,$email,$password,$id){
			$result = $this->func->myQuery("SELECT * FROM users WHERE id = ?","i",array($id),"fetch");

			if (!password_verify($password,$result['password']))
				return "password";

			return $this->func->myQuery("UPDATE users SET first_name = ?, last_name = ?, username = ?, email = ? WHERE id = ?","ssssi",array($fname,$lname,$uname,$email,$id),"action");
		}


	// PASSWORD CHANGE
		public function pass($old,$new,$id){
			$result = $this->func->myQuery("SELECT password FROM users WHERE id = ?","i",array($id),"fetch");

			if (!password_verify($old,$result['password']))
				return "password";

			$new = password_hash($new, PASSWORD_DEFAULT);

			return $this->func->myQuery("UPDATE users SET password = ? WHERE id = ?","si",array($new,$id),"action");
		}

		public function fetch_users(){
            $result = $this->func->myQuery("SELECT * FROM users WHERE role < ? ORDER BY first_name ASC","i",array(20),"result");

            if ($result->num_rows > 0)
                foreach ($result as $row)
                    $data[] = $row;
            else
                $data = false;

            return $data;
        }

        // Reset User
        public function reset_user($id){
            $tel = $this->func->myQuery("SELECT username FROM users WHERE id = ?", "i", array($id), "fetch");
            $tel = $tel['username'];
            $new = password_hash($tel, PASSWORD_DEFAULT);

            return $this->func->myQuery("UPDATE users SET password = ? WHERE id = ?","si",array($new,$id),"action");
        }

        // User Fetch
        public function fetch_user($id){
            return $this->func->myQuery("SELECT * FROM users WHERE id = ?","i",array($id),"fetch");
        }

        // Add User
        public function add_user($first,$last,$username,$email,$role,$status,$id){
            return $this->func->myQuery("INSERT INTO users(first_name,last_name,username,email,role,status,created_by) VALUES (?,?,?,?,?,?,?)","ssssisi",array($first,$last,$username,$email,$role,$status,$id),"action");
        }

        // User Status
        public function update_status_user($first,$last,$username,$email,$role,$status,$id){
            return $this->func->myQuery("UPDATE users SET first_name = ?, last_name = ?, username = ?, email = ?, role = ?, status = ? WHERE id = ?","ssssssi",array($first,$last,$username,$email,$role,$status,$id),"action");
        }

        // Record Audit
        public function record_audit($id,$msg){
            date_default_timezone_set("Africa/Accra");
            $dated = date("Y-m-d H:i:s");

            return $this->func->myQuery("INSERT INTO audit_trail(user_id,activity,date_done) VALUES (?,?,?)","iss",array($id,$msg,$dated),"action");
        }

	}