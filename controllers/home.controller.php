<?php
  /**
   * home dashboard
   */
  class Home
  {

    protected $func;
    public $single = null;
    public $individual;
    public $company;
    public $pending = [];
    public $events = [];

    function __construct()
    {
        $this->func = new myFunc;
    }

    public function index(){

      // individuals
      $result = $this->func->myQuery("SELECT COUNT(*) AS total FROM individuals WHERE 7 = ?","i",array(7),"fetch");
      $individual = $result['total'];

      // companies
      $result = $this->func->myQuery("SELECT COUNT(*) AS total FROM company WHERE 5 = ?","i",array(5),"fetch");
      $company = $result['total'];

      // screenings
      $result = $this->func->myQuery("SELECT COUNT(*) AS total FROM screenings WHERE 3 = ?","i",array(3),"fetch");
      $single = $result['total'];

      // pendings
      $result = $this->func->myQuery("SELECT * FROM screenings WHERE decided = ?","i",array(0),"result");
      // check if any results returned
      if($result->num_rows > 0)
        // put results in array
        foreach ($result as $row)
          $pending[] = $row;
      else
        // if no results returned, value is false
        $pending = false;

      return [$individual,$company,$single,$pending,false];
   }

  }

