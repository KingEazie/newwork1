<?php  

  class Login
  {
    protected $func;
    public $credentials = [];

    function __construct()
    {
      $this->func = new myFunc;
    }

    // LOGIN
    public function login($user,$password){
      $credentials = $this->func->myQuery("SELECT * FROM users WHERE username = ? OR email = ?","ss",array($user,$user),"fetch");
      if (password_verify($password,$credentials['password'])){
        return $credentials;
      }else{
        return false;
      }
    }

    // SIGNUP
    public function register($firstname,$lastname,$email,$username,$pass,$cpass){
      if ( $this->func->myQuery("SELECT * FROM users WHERE username = ? OR email = ?","ss",array($username, $email),"fetch") ) {
        return "present";
      } else { 
        date_default_timezone_set("Africa/Accra");
        $dated = date("Y-m-d");

        if ($pass !== $cpass) {
          return "pass";
        }else{
          $password = password_hash($pass,PASSWORD_DEFAULT);
          if ( $this->func->myQuery("INSERT INTO users (first_name, last_name, email, username, password, date_created, active) VALUES (?,?,?,?,?,?,?)","sssssss",array($firstname,$lastname,$email,$username,$password,$dated,'1'),"action") !== false ) {
              $id = $this->func->myQuery("SELECT id, first_name FROM users WHERE username = ?","s",array($username),"fetch");
              return $id;
          }
        }
      }
    }

    // CHANGE PASSWORD
    public function pass($old,$new,$id){
      $result = $this->func->myQuery("SELECT * FROM users WHERE id = ?", "i", array($id),"fetch");

      if (!password_verify($old,$result['password']))
        return "password";

      $new = password_hash($new, PASSWORD_DEFAULT);

      return $this->func->myQuery("UPDATE users SET password = ? WHERE id = ?","si",array($new,$id),"action");
    }

      // Record Audit
      public function record_audit($id,$msg){
          date_default_timezone_set("Africa/Accra");
          $dated = date("Y-m-d H:i:s");

          return $this->func->myQuery("INSERT INTO audit_trail(user_id,activity,date_done) VALUES (?,?,?)","iss",array($id,$msg,$dated),"action");
      }

  }
