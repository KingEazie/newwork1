<?php 
	/**
	* page controller class
	*/
	class Page
	{
		protected $func;
		public $infos = [];
        public $feeds = [];
        public $data = [];

		function __construct()
		{
			$this->func = new myFunc;
		}

	// AEJAY RANDOMISATION
	    public function aj_rands($c, $l, $u = FALSE) { 
	      if (!$u) for ($s = '', $i = 0, $z = strlen($c)-1; $i < $l; $x = rand(0,$z), $s .= $c{$x}, $i++); 
	      else for ($i = 0, $z = strlen($c)-1, $s = $c{rand(0,$z)}, $i = 1; $i != $l; $x = rand(0,$z), $s .= $c{$x}, $s = ($s{$i} == $s{$i-1} ? substr($s,0,-1) : $s), $i=strlen($s)); 
	      return $s; 
	    }  


	// DELIVERY KEY CREATION
	    public function rands($chars,$length){
	      $chars .= '0147852369';
	      do {
	        $key = self::aj_rands($chars,$length);
	        $check = $this->func->myQuery("SELECT code FROM ticket_purchases WHERE code = ?", "s", array($key),"fetch");
	        $check = $check['code'];
	      }while(!empty($check));
	      return $key;
	    }

		public function fetch_entities(){
		    // Individuals
		    $result = $this->func->myQuery("SELECT id, individual_id, first_name, middle_name, last_name, nationality, mobile, email, screenings, status FROM individuals WHERE 5 = ?","i",array(5),"result");
			if ($result->num_rows > 0)
				foreach ($result as $row)
					$infos[] = $row;
			else
				$infos = false;

			// Company
            $result = $this->func->myQuery("SELECT id, company_id, company_name, telephone, email, business_purpose, screenings, status FROM company WHERE 7 = ?","i",array(7),"result");
            if ($result->num_rows > 0)
                foreach ($result as $row)
                    $feeds[] = $row;
            else
                $feeds = false;

            // Nationality
            $result = $this->func->myQuery("SELECT * FROM nationality WHERE 7 = ?","i",array(7),"result");
            if ($result->num_rows > 0)
                foreach ($result as $row)
                    $data[] = $row;
            else
                $data = false;

			return [$infos,$feeds,$data];
		}

		public function fetch_nationality(){
            $result = $this->func->myQuery("SELECT * FROM nationality WHERE 7 = ? ORDER BY id ASC","i",array(7),"result");
            if ($result->num_rows > 0)
                foreach ($result as $row)
                    $data[] = $row;
            else
                $data = false;

            return $data;
        }

		public function fetch_screenings(){
			$result = $this->func->myQuery("SELECT * FROM screenings WHERE 7 = ? ORDER BY date_done ASC","i",array(7),"result");
            if ($result->num_rows > 0)
                foreach ($result as $row)
                    $infos[] = $row;
            else
                $infos = false;

            return $infos;
		}

        public function fetch_trail(){
            $result = $this->func->myQuery("SELECT a.*, u.username FROM audit_trail a JOIN users u on a.user_id = u.id WHERE 5 = ? ORDER BY date_done DESC","i",array(5),"result");
            if ($result->num_rows > 0)
                foreach ($result as $row)
                    $infos[] = $row;
            else
                $infos = false;

            return $infos;
        }

		public function fetch_tasks($id){
            $result = $this->func->myQuery("SELECT sr.subject, rf.field_name FROM staff_requests sr JOIN request_fields rf on r.field_id = rf.id WHERE request_with = ? AND (status != 2 OR status != -1) ORDER BY id ASC","i",array($id),"result");
            if ($result->num_rows > 0)
                foreach ($result as $row)
                    $infos[] = $row;
            else
                $infos = false;

            return $infos;
        }

        // Create Individual
        public function create_individual($id,$firstname,$middlename,$lastname,$gender,$dob,$nationality,$mobile,$email,$user){
		    $check = $this->func->myQuery("SELECT individual_id FROM individuals WHERE email = ?","s",array($email),"fetch");
            if (isset($check['individual_id'])) {
                return "present";
            } else {
                date_default_timezone_set("Africa/Accra");
                $dated = date("Y-m-d");

                return $this->func->myQuery("INSERT INTO individuals (individual_id, first_name, middle_name, last_name, gender, dob, nationality, mobile, email, date_created, created_by) VALUES (?,?,?,?,?,?,?,?,?,?,?)","ssssssssssi",array($id,$firstname,$middlename,$lastname,$gender,$dob,$nationality,$mobile,$email,$dated,$user),"action");
            }
        }

        // Edit Individual
        public function edit_individual($id,$gender,$dob,$nationality,$mobile,$email){
		    if (is_null($dob))
                return $this->func->myQuery("UPDATE individuals SET gender = ?, nationality = ?, mobile = ?, email = ? WHERE individual_id = ?","sssss",array($gender,$nationality,$mobile,$email,$id),"action");
            else
                return $this->func->myQuery("UPDATE individuals SET gender = ?, dob = ?, nationality = ?, mobile = ?, email = ? WHERE individual_id = ?","ssssss",array($gender,$dob,$nationality,$mobile,$email,$id),"action");
		}

        // Create Company
        public function create_company($id,$name,$telephone,$email,$business,$inc_number,$inc_type,$inc_country,$user){
            if ( $this->func->myQuery("SELECT id FROM company WHERE email = ?","s",array($email),"fetch") ) {
                return "present";
            } else {
                date_default_timezone_set("Africa/Accra");
                $dated = date("Y-m-d");

                return $this->func->myQuery("INSERT INTO company (company_id, company_name, telephone, email, business_purpose, incorporation_number, incorporation_type, incorporation_country, date_created, created_by) VALUES (?,?,?,?,?,?,?,?,?,?)","sssssssssi",array($id,$name,$telephone,$email,$business,$inc_number,$inc_type,$inc_country,$dated,$user),"action");
            }
        }

        // Edit Company
        public function edit_company($id,$telephone,$email,$business,$inc_number,$inc_type,$inc_country){
            return $this->func->myQuery("UPDATE company SET telephone = ?, email = ?, business_purpose = ?, incorporation_number = ?, incorporation_type = ?, incorporation_country = ?  WHERE company_id = ?","sssssss",array($telephone,$email,$business,$inc_number,$inc_type,$inc_country,$id),"action");
        }

        // Fetch Individual
        public function fetch_individual($id){
            return $this->func->myQuery("SELECT * FROM individuals WHERE individual_id = ?","i",array($id),"fetch");
        }

        // Fetch Company
        public function fetch_company($id){
            return $this->func->myQuery("SELECT * FROM company WHERE company_id = ?","i",array($id),"fetch");
        }

        // Screen Count Add Up
        public function individual_add_screen($id){
            return $this->func->myQuery("UPDATE individuals SET screenings = screenings + 1 WHERE id = ?","i",array($id),"action");
        }

        public function company_add_screen($id){
            return $this->func->myQuery("UPDATE company SET screenings = screenings + 1 WHERE id = ?","i",array($id),"action");
        }

        // Screening Approvals
        public function approve_individual($cid,$sid,$ostatus,$outcome,$mid,$decision,$user){
            date_default_timezone_set("Africa/Accra");
            $dated = date("Y-m-d");

            if (!empty($decision))
                $this->individual_set_status($decision,$cid);

            return $this->func->myQuery("INSERT INTO screenings(customer_id,screen_type,screening_id,overall_status,outcome,match_id,decided,date_done,done_by) VALUES (?,?,?,?,?,?,?,?,?)","ssssssisi",array($cid,'INDIVIDUAL',$sid,$ostatus,$outcome,$mid,$decision,$dated,$user),"action");
        }

        public function approve_company($cid,$sid,$ostatus,$outcome,$mid,$decision,$user){
            date_default_timezone_set("Africa/Accra");
            $dated = date("Y-m-d");

            if (!empty($decision))
                $this->company_set_status($decision,$cid);

            return $this->func->myQuery("INSERT INTO screenings(customer_id,screen_type,screening_id,overall_status,outcome,match_id,decided,date_done,done_by) VALUES (?,?,?,?,?,?,?,?,?)","ssssssisi",array($cid,'COMPANY',$sid,$ostatus,$outcome,$mid,$decision,$dated,$user),"action");
        }

        // Set Status
        public function individual_set_status($status,$id){
            return $this->func->myQuery("UPDATE individuals SET status = ? WHERE individual_id = ?","ss",array($status,$id),"action");
        }

        public function company_set_status($status,$id){
            return $this->func->myQuery("UPDATE company SET status = ? WHERE company_id = ?","ss",array($status,$id),"action");
        }

        public function update_screen($status,$id){
            return $this->func->myQuery("UPDATE screenings SET decided = ? WHERE id = ?","ii",array($status,$id),"action");
        }

        // Record Audit
        public function record_audit($id,$msg){
            date_default_timezone_set("Africa/Accra");
            $dated = date("Y-m-d H:i:s");

            return $this->func->myQuery("INSERT INTO audit_trail(user_id,activity,date_done) VALUES (?,?,?)","iss",array($id,$msg,$dated),"action");
        }

        // Miscellaneous
		public function send_mail($to,$subject,$message){
			return $this->func->sendmail($to,$subject,$message);
		}

	}
