<?php  

    class myFunc{
        public static function myQuery($query, $datatype, $variables, $type){
            include("db.php");
            $stmt = $conn->prepare($query);
            $param = array();
            $count = count($variables);
            for($i = 0; $i < $count; ++$i){
                $param[] = &$variables[$i];
            }
            array_unshift($param, $datatype);
            call_user_func_array(array($stmt, 'bind_param'), $param);
            $execute = $stmt->execute();
            if ($type == 'result') {
                $result = $stmt->get_result();
                $stmt->close();
                $conn->close();
                return $result;
            }elseif ($type == 'fetch') {
                $result = $stmt->get_result();
                $stmt->close();
                $conn->close();
                return $result->fetch_assoc();
            }elseif ($type == 'action') {
                if ($execute == false) {
                    echo $stmt->error;
                    $stmt->close();
                    $conn->close();
                    return false;
                }else{
                    $stmt->close();
                    $conn->close();
                    return true;
                }
            }
        }

        // my email function
        public static function sendmail($to,$subject,$message){
            $from = 'no-reply@paymentsareus.com';
            // headers
            $headers = "From: " . strip_tags($from) . "\r\n";
            $headers .= "MIME-Version: 1.0\r\n";
            $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
              
            //send email
            mail($to, $subject, $message, $headers);
        }

        // my sms function
        public static function sendsms($phone,$message){
            $url = 'https://shop.digitaltermination.com/api/instntmny-local/in-house/transactions/status/';
            $data = array(
                "message" => $message,
                "recipient" => [$phone]
              );
    
            $data = json_encode($data);
            $response = self::aj_zp_post('POST',$url,$data);
            return $response;  
        }        

        // my salt function.
        public static function cryptPass($input, $rounds = 9){
            $salt = "";

            $saltChars = array_merge(range('A', 'Z'), range('a', 'z'), range(0, 9));

            for($i = 0; $i < 22; $i++){
                $salt .= $saltChars[array_rand($saltChars)];
            }

            return crypt($input, sprintf('$2y$%02d$', $rounds) . $salt);
        }

        public static function imgUpload($path,$imgName,$imgTemp,$imgSize){ 
            $target_dir = $path;
            $target_file = $target_dir . basename($imgName);
            $uploadOk = 1;
            $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
            // Check if image file is a actual image or fake image
                $check = getimagesize($imgTemp);
                if($check !== false) {
                    $uploadOk = 1;
                } else {
                    $uploadOk = 0;
                    return false;
                }
            // Check if file already exists
            if (file_exists($target_file)) {
                $uploadOk = 0;
                return true;
            }
            // Check file size
            if ($imgSize > 1000000) {
                $uploadOk = 0;
                return false;
            }
            // Allow certain file formats
            if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
            && $imageFileType != "gif" ) {
                $uploadOk = 0;
                return false;
            }
            // Check if $uploadOk is set to 0 by an error
            if ($uploadOk == 0) {
            // if everything is ok, try to upload file
                return false;
            } else {
                if (move_uploaded_file($imgTemp, $target_file)) {
                    return true;
                } else {
                    return false;
                }
            }
        }

        public static function timeAgo($timestamp){
            $datetime1=new DateTime("now");
            $datetime2=date_create($timestamp);
            $diff=date_diff($datetime1, $datetime2);
            $timemsg='';
            if($diff->y > 0){
                $timemsg = $diff->y .' year'. ($diff->y > 1?"s":'');

            }
            else if($diff->m > 0){
             $timemsg = $diff->m . ' month'. ($diff->m > 1?"s":'');
            }
            else if($diff->d > 0){
             $timemsg = $diff->d .' day'. ($diff->d > 1?"s":'');
            }
            else if($diff->h > 0){
             $timemsg = $diff->h .' hour'.($diff->h > 1 ? "s":'');
            }
            else if($diff->i > 0){
             $timemsg = $diff->i .' minute'. ($diff->i > 1?"s":'');
            }
            else if($diff->s > 0){
             $timemsg = $diff->s .' second'. ($diff->s > 1?"s":'');
            }

            $timemsg = $timemsg.' ago';
            return $timemsg;
        }

        public static function aj_all_get($method,$url,$data){
            // Initialisation
            $ch = curl_init();
            switch ($method) {
                case 'POST':
                    curl_setopt($ch, CURLOPT_POST, 1);
                    if ($data)
                        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
                    break;
                case 'PUT':
                    curl_setopt($ch, CURLOPT_POST, 'PUT');
                    if ($data)
                        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
                    break;                                    
                default:
                    if ($data)
                        $url = sprintf('%s?%s',$url,http_build_query($data));
                    break;
            }
            // Options
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
            // Execute
            $response = curl_exec($ch);
            if (!$response)
                die('Connection Failure!');
            curl_close($ch);
            // Results 
            $json = json_decode($response, true);
            return $json;
        }

        public static function auto_link($text) {
          $pattern = '/(((http[s]?:\/\/(.+(:.+)?@)?)|(www\.))[a-z0-9](([-a-z0-9]+\.)*\.[a-z]{2,})?\/?[a-z0-9.,_\/~#&=:;%+!?-]+)/is';
          $text = preg_replace($pattern, ' <a href="$1" target="_blank" rel="nofollow">$1</a>', $text);
          // fix URLs without protocols
          $text = preg_replace('/href="www/', 'href="http://www', $text);
          return $text;
        }

        public static function aj_zp_post($method,$url,$data,$token){
            // Initialisation
            $ch = curl_init();
            // Options
            curl_setopt_array($ch, array(
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => $method,
                CURLOPT_POSTFIELDS => $data,
                CURLOPT_HTTPHEADER => array(
                    "authorization: $token",
                    "accept: application/json",
                    "cache-control: no-cache",
                    "content-type: application/json"
                ),
                CURLOPT_SSL_VERIFYPEER => false
            ));
            // Execution
            $response = curl_exec($ch);
            $error = curl_error($ch);
            curl_close($ch);
            // Results 
            if ($error) {
                echo 'Error: '.$error;
                return 0;
            }else{
                $json = json_decode($response, true);
                return $json;
            }
        }

    public static function aj_zp_login($url,$data){
        // Initialisation
        $ch = curl_init();
        // Options
        curl_setopt_array($ch, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => $data,
            CURLOPT_HTTPHEADER => array(
                'cache-control: no-cache',
                'content-type: application/x-www-form-urlencoded'
            ),
            CURLOPT_SSL_VERIFYPEER => false
        ));
        // Execution
        $response = curl_exec($ch);
        $error = curl_error($ch);
        curl_close($ch);
        // Results
        if ($error) {
            echo 'Error: '.$error;
            return 0;
        }else{
            //$json = json_decode($response, true);
            return $response;
        }
    }

    public static function aj_zp_get($url,$token){
        // Initialisation
        $ch = curl_init();
        // Options
        curl_setopt_array($ch, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => array(
                    "authorization: $token",
                    "accept: application/json",
                    "cache-control: no-cache",
                    "content-type: application/json"
            ),
            CURLOPT_SSL_VERIFYPEER => false
        ));
        // Execution
        $response = curl_exec($ch);
        $error = curl_error($ch);
        curl_close($ch);
        // Results
        if ($error) {
            echo 'Error: '.$error;
            return 0;
        }else{
            $json = json_decode($response, true);
            return $json;
        }
    }

    }
?>