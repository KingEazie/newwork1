$(document).ready(function(){        

   // simple ajax calls function
  function myAjax(t, n, o, type = 'post') {
      var e = '';
      if (type == 'post') {
          var id = $(t).attr("id");
          var query = $(t).attr("data-query");
          var value = $(t).attr("data-value");
          var e = 'id='+ encodeURIComponent(id) + '&query='+ encodeURIComponent(query) + '&value='+ encodeURIComponent(value);
      }else if(type == 'form'){
          var e = new FormData(t);

      }

      if (type == 'post') {
          $.ajax({
              type: "POST",
              url: n,
              data: e,
              dataType: "html",
              beforeSend: function() {
                  $(o).html('Processing ...');
              },
              success: function(t) {
                  $(o).html(t).show();
              }
          });
      }else if (type == 'form'){
          $.ajax({
              type: "POST",
              url: n,
              data: e,
              cache: !1,
              contentType: !1,
              processData: !1,
              beforeSend: function() {
                  $(o).html('Processing ...');
              },
              success: function(t) {
                  $(o).html(t).show();
              },
              error: function(t) {
                  console.log("something's wrong")
              }
          });
      }
  }

    /*specific form ajax calls*/
    $(document).on("submit", ".form", function(e){
        e.preventDefault();
        $output = $(this).attr("data-output");
        $dest = $(this).attr("data-dest");
        $checkout = $(this).attr("data-checkout");
        $type = 'form';

        myAjax(this, $dest, $output, $type);

        if ($(this).attr("data-toggle") == 'modal') {
            $("#modal").modal("show");
        }

        if ($(this).attr("data-clear-input") == 'true') {
            $(this).find("input,textarea").val("");
        }

    });

/*end of function*/

  /*specific calls and output*/
  $(document).on("click", ".spec-ajax", function(e){
      e.preventDefault();
      $output = $(this).attr("data-output");
      $dest = $(this).attr("data-dest");
      $trigger = $(this).attr("data-trigger");
      $fadeOut = $(this).attr("data-fadeOut");
      $extend = $(this).attr("data-extend-view");
      $parent = $(this).attr("data-parent");
      $title = $(this).attr("data-title");
      $return = $(this).attr("return");

      if (typeof($trigger) != 'undefined' || $trigger != null) {
          $($trigger).trigger("click");
      }

      if (typeof($extend) != 'undefined' || $extend != null) {
          $source = $(this).parents($parent).find($extend);
          $($output).html($($source).html());

          if ($(this).attr("data-toggle") == 'modal') {
              if (typeof($title) != 'undefined' || $title != null)
                  $(".modal-title").html($title);

              $("#modal").modal("show");
          }
      }

      if (typeof($return) != 'undefined' || $return != null)
            return true;      

      myAjax(this, $dest, $output);

      if ($(this).attr("data-toggle") == 'modal') {
          if (typeof($title) != 'undefined' || $title != null)
              $(".modal-title").html($title);

          $("#modal").modal("show");
      } 

      if(typeof($fadeOut) != 'undefined' || $fadeOut != null){
          $(this).parents($fadeOut).fadeOut("slow").html("");
      }

  });

 $(document).on("click",".menu",function(){
  $('.active-menu').removeClass("active-menu");
  $(this).addClass("active-menu");
 });

$(document).on("submit", "#feedform", function(e){
  e.preventDefault();
  myFormAjax(this,"../feedback/index.php","#feedback");
  $("#myModal").modal("show");

  $("#reset").click();

  setInterval(function(){ $("#myModal").modal("hide"); $("#feedback").html(""); }, 20000);
});

    // popover initialisation
    $('[data-toggle="popover"]').popover();

});